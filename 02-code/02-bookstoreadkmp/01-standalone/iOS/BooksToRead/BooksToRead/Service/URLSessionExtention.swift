//
//  URLSessionExtention.swift
//  BooksToRead
//
//  Created by Mohamed Ben Hajla on 15.08.19.
//  Copyright © 2019 Mohamed Ben Hajla. All rights reserved.
//

import Foundation

import Combine

enum RequestError: Error {
    case request(code: Int, error: Error?)
    case cannotParse
    case unknown
}

enum APIError: Error, LocalizedError {
    case unknown, apiError(reason: String)
    var errorDescription: String? {
        switch self {
            case .unknown:
        return "Unknown error"
            case .apiError(let reason):
        return reason
            }
        }
}

extension URLSession {
    func send(request: URLRequest) -> AnyPublisher<Data, Error> {
        return URLSession.shared.dataTaskPublisher(for: request)
            .receive(on: DispatchQueue.main)
            .mapError({ (e) -> Error in
                return e
            })
            .tryMap { data, response -> Data in
                let httpResponse = response as? HTTPURLResponse
                if let httpResponse = httpResponse, 200..<300 ~= httpResponse.statusCode {
                    return data
                }
                else if let httpResponse = httpResponse {
                    throw RequestError.request(code: httpResponse.statusCode, error: NSError(domain: httpResponse.description, code: httpResponse.statusCode, userInfo: httpResponse.allHeaderFields as? [String : Any]))
                }     else {
                    throw RequestError.unknown
                }
           
            }.mapError { error in
            if let error = error as? APIError {
            return error
                        } else {
            return APIError.apiError(reason: error.localizedDescription)
                        }
                    }
            .eraseToAnyPublisher()
    }
}
