//
//  BookModel.swift
//  BooksToRead
//
//  Created by Mohamed Ben Hajla on 13.08.20.
//  Copyright © 2020 Mohamed Ben Hajla. All rights reserved.
//

import Foundation

struct Book: Codable, Identifiable {
    let id: String
    let title: String
    let authors: [String]
    let notes: String
    let bookCategory: String
    let bookCategoryImageName: String
    let description: String
    let thumbnail: String?
    //let webLink: String?
    
}

