//
//  ContentView.swift
//  BooksToRead
//
//  Created by Mohamed Ben Hajla on 13.08.20.
//  Copyright © 2020 Mohamed Ben Hajla. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var viewModel = ViewModel()
    
    var body: some View {
           NavigationView{
                     List(viewModel.books,  id: \.title) { book in
                        Text(book.title)
                         
                         }.navigationBarTitle(Text("Book Picks")).font(.subheadline).listStyle(GroupedListStyle())
                 }.onAppear {
                     self.viewModel.fetchBooks()
                    
                 }
    }
}

/*
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
*/
