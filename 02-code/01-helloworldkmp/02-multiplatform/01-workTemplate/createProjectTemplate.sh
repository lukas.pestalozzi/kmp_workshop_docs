#!/bin/bash

#***************Folders ************************

#create top level folders for android, ios and shared subprojects
mkdir -p shared
mkdir -p androidApp
mkdir -p iosApp
mkdir -p jsApp

#********** shared subproject *****************************

#create  Sub-folders for the shared project
mkdir -p shared/src/commonMain/kotlin
mkdir -p shared/src/jvmMain/kotlin
mkdir -p shared/src/iosMain/kotlin
mkdir -p shared/src/jsMain/kotlin
mkdir -p shared/src/main

# Create the source Files and the build gradle File

touch shared/src/commonMain/kotlin/common.kt
touch shared/src/jvmMain/kotlin/android.kt
touch shared/src/iosMain/kotlin/ios.kt
touch shared/src/jsMain/kotlin/js.kt
touch shared/src/main/AndroidManifest.xml
touch shared/build.gradle 



#********** iosApp subproject *****************************
# will be create with XCode IDE in the iosApp sub-directory



#********** androidApp subproject *****************************
# will be create with Android IDE in the androidApp sub-directory



#******** jsApp subproject *******************************

#create  Sub-folders for the jsApp project
mkdir -p jsApp
mkdir -p jsApp/src/main/kotlin
mkdir -p jsApp/src/main/resources
mkdir -p jsApp/src/main/kotlin/io.sunnyside.hellokmp


# Create the source Files and the build gradle File

touch jsApp/src/main/kotlin/io.sunnyside.hellokmp/main.kt
touch jsApp/src/main/resources/index.html
touch jsApp/src/main/resources/styles.css
touch jsApp/build.gradle
